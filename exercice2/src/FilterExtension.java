import java.io.File;
import java.io.FilenameFilter;
import java.util.Scanner;

public class FilterExtension {

    private static Scanner scanDir = new Scanner(System.in);
    private static Scanner scanExt = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Veuillez taper votre chemin d'acces:");
        String directory = scanDir.nextLine();

        System.out.print("Veuillez taper votre extension:");
        String extension = scanExt.nextLine();

        String dir = directory;
        String ext = ("." + extension);

        findFiles(dir, ext);
    }

    private static void findFiles(String dir, String ext) {
        File file = new File(dir);
        if (!file.exists())
            System.out.println(dir + " Ce chemin n'existe pas");
        File[] listFiles = file.listFiles(new MyFileNameFilter(ext));


        if (listFiles.length == 0) {
            System.out.println(dir + "ne possède pas de fichier avec l'extension " + ext);
        } else {
            for (File f : listFiles)
                System.out.println("Fichier: " + dir + File.separator + f.getName());
        }
    }


    public static class MyFileNameFilter implements FilenameFilter {

        private String ext;

        public MyFileNameFilter(String ext) {
            this.ext = ext.toLowerCase();
        }

        @Override
        public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(ext);
        }

    }

}
