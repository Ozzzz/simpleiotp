import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import static java.lang.System.getProperty;

public class WriteAll {


    public static void main(String args[]) {

       try {
           String file = "test.txt";

           Scanner scan = new Scanner(System.in);

           FileWriter fw = new FileWriter(file);
           System.out.println("Ecrivez ce que vous voulez");

           while (true) {
               String input = scan.nextLine();
               if ("quit".equalsIgnoreCase(input.trim())) {
                   break;
               }
               fw.write(input+getProperty("line.separator"));
           }
           scan.close();
           System.out.println("Modifications enregistrees");
           fw.close();
       } catch (IOException e) {
           System.out.println("Erreur");
       }
    }
}
