import java.io.IOException;
import java.util.Scanner;
import java.io.File;
import java.nio.file.Files;

/* Exercice 1 et 3 */

public class ListBuild {


    private static Scanner scanner = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.print("Veuillez taper votre chemin d'acces:");
        String directory = scanner.nextLine();

        try {
            Files.list(new File(directory).toPath())
                    .limit(50)
                    .forEach(path -> {
                        System.out.println(path);
                    });

        } catch (IOException e) {
          System.out.print("Le chemin d'acces : " + directory + "est incorrect");
        }

    }

}

